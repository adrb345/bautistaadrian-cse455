package com.example.adrian.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    String url = "http://api.fixer.io/latest?base=USD"; //currency_url
    String json = ""; //json_currency

    String line = "";
    //rate is the target
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //cast variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        //Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                BackgroundTask object = new BackgroundTask();
                object.execute();
                /*
                usd = editText01.getText().toString();
                if(usd.equals("")) {
                    textView01.setText("This fiend cannot be blank!");
                } else {
                    Double dInputs = Double.parseDouble(usd);
                    Double result = dInputs * 112.57;
                    textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));
                    editText01.setText("");
                }
                */
            }
        });
    }
    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(String results) {
            super.onPostExecute(results);
        }
        @Override
        protected String doInBackground(Void... params) {

            try {
                URL web_url = new URL(MainActivity.this.url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();


                httpURLConnection.setRequestMethod("GET");

                httpURLConnection.connect();

                //System.out.println("CONNECTION SUCCESSFUL");

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESSFUL");

                while(line != null) {
                    line = bufferedReader.readLine();
                    json += line;
                }

                System.out.println("\nTHE JSON: " + json);

                JSONObject obj = new JSONObject(json);

                JSONObject objRate = obj.getJSONObject("rates");
                rate = objRate.get("JPY").toString();

                System.out.println("What is rate: " + rate + "\n");

                final Double rateDouble = Double.parseDouble(rate);
                System.out.println("\nTesting JSON String Exchange Rate INSIDE AsyncTask : " + rateDouble);

                /**Currency Code below, async and json above**/

                // runOnUiThread function used to fix error that crashes app when trying to update the main ui
                runOnUiThread(new Runnable() {
                    public void run() {

                        usd = editText01.getText().toString();

                        if (usd.equals("")) {
                            textView01.setText("This fiend cannot be blank!");
                        } else {
                            Double dInputs = Double.parseDouble(usd);
                            //Double result = dInputs * 112.57;
                            Double result = dInputs * rateDouble;
                            textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));

                            editText01.setText("");
                        }
                    }
                });

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);

                System.exit(1);
            }


            return null;
        }
    }
}
