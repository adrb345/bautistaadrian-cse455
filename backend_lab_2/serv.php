<?php

$host = 'localhost';
$username = 'root';
$password = '';
$dbname = 'cse455';

$dsn = 'mysql:host='. $host .';dbname='. $dbname;

$name = $_GET['name'];
$amount = $_GET['amount'];

try {
    $pdo = new PDO($dsn, $username, $password);
    
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $sql = 'SELECT currency_rate FROM currency WHERE currency_name=?';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$name]);
    $posts = $stmt->fetch(PDO::FETCH_OBJ);
    
    //removes warning about having empty object
    $conObj = new \stdClass();
    
    $conObj->conversion = $posts->currency_rate * $amount;
    echo json_encode($conObj);
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>
